#!/bin/bash
##Create VM Debian 10 amd64


mkdir d10
cd d10


wget https://cloud.debian.org/images/cloud/buster/20210721-710/debian-10-generic-amd64-20210721-710.tar.xz


tar -xpJf debian-10-generic-amd64-20210721-710.tar.xz

qemu-img convert -f raw -O qcow2 disk.raw d10.qcow2

qemu-img resize d10.qcow2 +4G

mv d10.qcow2 /var/lib/libvirt/images/d10.qcow2


virt-sysprep -v -a /var/lib/libvirt/images/d10.qcow2 \
--run-command 'dpkg-reconfigure openssh-server' \
--root-password password:password \
--ssh-inject root:file:/home/user/.ssh/id_rsa.pub 

virt-install --connect qemu:///system \
--import \
--name d10amd64 \
--memory 1024 \
--network bridge:virbr1,model=virtio \
--os-variant debian10 \
--disk /var/lib/libvirt/images/d10.qcow2 \
--vcpus 1 \
--vnc \
--noautoconsole

exit 0
