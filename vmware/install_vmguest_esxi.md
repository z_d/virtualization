### Install ovftool on Windows. 

https://docs.vmware.com/en/VMware-Telco-Cloud-Operations/1.0/deployment-guide-10/GUID-95301A42-F6F6-4BA9-B3A0-A86A268754B6.html

https://code.vmware.com/web/tool/4.4.0/ovf

https://vmware.github.io/vic-product/assets/files/html/1.5/vic_vsphere_admin/deploy_vic_appliance_ovftool.html#example-ovftool-command

https://vdc-download.vmware.com/vmwb-repository/dcr-public/4f19cf95-19aa-4b28-bc8a-2d1bc80a122c/5aecb578-98dc-4e10-9c18-d118cacd4fa1/ovftool-440-userguide.pdf

### Example #1

For Windows 10.

Install Debian 11 with Gnome Desktop.

With login/password debian/debian.

```
wget https://techloudgeek.com/download/image/?link=https://dlhz157.linuxvmimages.com/VMware/D/11/Debian_11.0.0_VM.7z

!Use archivator for open archive-file!

cd C:\Program Files\VMware\VMware OVF Tool

.\ovftool.exe --acceptAllEulas C:\Users\user\Downloads\Debian_11.0.0_VM\Debian_11.0.0_VM_LinuxVMImages.COM.vmx C:\Users\user\Desktop\debian.ova

.\ovftool.exe --noSSLVerify --acceptAllEulas -ds=datastore1 --name=Debian  --numberOfCpus:'*'=1 --memorySize:'*'=1024 --diskMode=thin "C:\Users\user\Desktop\debian.ova"  "vi://root:Password@ip-address"

```

### Example #2

Install Ubuntu Server use convert image.

```
mkdir ubuntu

cd ubuntu

wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.ova

tar -xvf focal-server-cloudimg-amd64.ova

rm focal-server-cloudimg-amd64.ova

qemu-img convert -O qcow2 ubuntu-focal-20.04-cloudimg.vmdk ubuntu-focal-20.04-cloudimg.qcow2

rm ubuntu-focal-20.04-cloudimg.vmdk 

sudo virt-sysprep -v -a ubuntu-focal-20.04-cloudimg.qcow2 \
--run-command 'dpkg-reconfigure openssh-server' \
--root-password password:password \
--ssh-inject root:file:/home/user/.ssh/id_rsa.pub 

qemu-img convert -f qcow2 -O vmdk ubuntu-focal-20.04-cloudimg.qcow2 ubuntu-focal-20.04-cloudimg.vmdk

rm ubuntu-focal-20.04-cloudimg.qcow2 

sudo chmod -R 777 ubuntu
```

Create ova file. 

There may be checksum errors in the mf-file and an error with the disk size in the ovf-file.

It is corrected by editing and entering the necessary values and checksums. -> Error: SHA digest of file ubuntu-focal-20.04-cloudimg.ovf does not match manifest

Use tool sha256sum in linux command line -> 
```
# sha256sum disk.vmdk 
# sha256sum file.ovf
``` 
```
ovftool --acceptAllEulas ubuntu-focal-20.04-cloudimg.ovf ubuntu-focal-20.04-cloudimg.ova

sudo chmod 777 ubuntu-focal-20.04-cloudimg.ova
```

Deploy ova-image to ESXi-host.

```
ovftool --noSSLVerify -ds=datastore1 --name=Test ubuntu-focal-20.04-cloudimg.ova  "vi://root:password@ip-address"  

```

### Example #3

For Linux.

Install Debian 10 Cloud-image. 

```
mkdir images

cd images

wget https://cloud.debian.org/images/cloud/buster/latest/debian-10-generic-amd64.qcow2

sudo virt-sysprep -v -a debian-10-generic-amd64.qcow2 \
--run-command 'dpkg-reconfigure openssh-server' \
--root-password password:password \
--ssh-inject root:file:/home/user/.ssh/id_rsa.pub 

qemu-img convert -f qcow2 -O vmdk debian-10-generic-amd64.qcow2 debian10amd64.vmdk

```
Create directory 'images' on ESXi-host.
```
mkdir /vmfs/volumes/datastore1/images
```

Copy vmdk-file to ESXi-host.
```
scp debian10amd64.vmdk root@192.168.1.50:/vmfs/volumes/datastore1/images

vmkfstools -i debian10amd64.vmdk -d thin d10.vmdk

```
Create manually VM-guest and attach vmdk-file.


### Example #4

For Linux.
```
mkdir ubuntu

cd ubuntu

wget https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.ova

ovftool --acceptAllEulas --powerOn --noSSLVerify -ds=datastore1 --nic:"ipAddressingMode=DHCP" --X:injectOvfEnv --X:enableHiddenProperties --prop:password="password" --prop:public-keys="ssh-rsa xxxxxx" --name=Test --overwrite focal-server-cloudimg-amd64.ova "vi://root:password@ip-address"

```
For Windows.
```
cd "C:\Program Files\VMware\VMware OVF Tool\"

mkdir ubuntu

Invoke-WebRequest  https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.ova -OutFile "C:\Program Files\VMware\VMware OVF Tool\ubuntu\ubuntu-focal.ova"

.\ovftool.exe --acceptAllEulas --powerOn --noSSLVerify -ds=datastore1 --nic:"ipAddressingMode=DHCP" --X:injectOvfEnv --X:enableHiddenProperties --prop:password="Password" --prop:public-keys="ssh-rsa xxxxx" --prop:hostname="Kubernetes" --name=Kubernetes --numberOfCpus:*=1 --overwrite "C:\Program Files\VMware\VMware OVF Tool\ubuntu\ubuntu-focal.ova" "vi://root:Password@ip-address"

```
