# virtualization


Examples installation VM-guest on:

- [VmWare/Esxi](https://gitlab.com/z_d/virtualization/-/tree/main/vmware)

- [KVM](https://gitlab.com/z_d/virtualization/-/tree/main/kvm) 

- [Vagrant](https://gitlab.com/z_d/virtualization/-/tree/main/vagrant)
