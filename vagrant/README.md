#### Example use Vagrant

##### Example use Vagrant and Virtualbox on Windows 10

- Install Virtualbox

- Install Vagrant

Open PowerShell.

Create directory
> mkdir vagrant_test

> cd .\vagrant_test\

Init and creating Vagrantfile

> vagrant init generic/centos7

> vagrant box add generic/centos7

Choise provider  - 5) virtualbox

List

> vagrant box list

Creating Guest-VM

> vagrant up

Connecting to Guest-VM on ssh

> vagrant ssh

Destroy Guest-VM

> vagrant destroy






